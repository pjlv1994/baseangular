import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  textForm : string = 'Crear Cuenta';
  active : boolean = false;
  constructor() { }

  ngOnInit() {
  }
  Active(){
    this.active = true;
    console.log(this.active)
  }

}
