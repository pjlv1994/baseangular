import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.sass']
})
export class PaymentsComponent implements OnInit {
  slides = [
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 1",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 2",
    },    
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 3",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 4",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 5",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 6",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 7",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 8",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 9",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 10",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 11",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 12",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 13",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 14",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "contrato 15",

    },

  ];
  slideConfig = {
    "slidesToShow": 8, "slidesToScroll": 5, 
    'responsive': [
      {
        'breakpoint': 767,
         'settings': {
          'slidesToShow': 3
                }
              }
            ]
};
  constructor() { }

  ngOnInit() {
  }

}
