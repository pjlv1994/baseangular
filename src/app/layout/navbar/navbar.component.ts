import { Component, OnInit } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
  textBtn : string = 'Crear Cuenta';
  loading: boolean = false;
  iconLoading = icon.faSpinner;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  register(){
    this.loading = true;
    this.router.navigate(['/register']);
    
  }

}
