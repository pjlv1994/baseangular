import { Component, OnInit } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  lock = icon.faLock
  longArrowRight = icon.faLongArrowAltRight
  textForm : string = 'Ingresar';
  icon : any;
  loading: boolean = false;
  iconLoading = icon.faSpinner;
  constructor () { }
  ngOnInit() {
    this.icon = this.longArrowRight;
  }
  ngOnSubmit(){
    this.loading = true;
    this.icon = this.iconLoading
  }



}
