import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.sass']
})
export class PanelComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  mostrar(){
    let sidebar = document.querySelector('#sidebar')
    sidebar.classList.toggle('active')
    // console.log('mostrar')
  }
  closet(){
    localStorage.clear()
    this.router.navigate(['/login']);
  }
}
